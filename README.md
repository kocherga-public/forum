# Форум для обсуждения недостатков и улучшений [Кочерги](https://kocherga-club.ru/)

Чтобы создать обсудить новую тему, [создайте issue](https://gitlab.com/kocherga-public/forum/issues/new).

Заводим по одному issue на одну тему, от «куда делись мои любимые печеньки сорта X?» до «позовите Илона Маска прочитать лекцию».

Список всех issue: https://gitlab.com/kocherga-public/forum/issues